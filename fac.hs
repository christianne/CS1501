-- fac.hs
-- Write 2 different implementations of factorial

-- My answer:
facA n = product [1..n]

-- Understanding this:
--  $ is not applicable
facB n = if n > 1
         then n * facB(n-1)
         else n